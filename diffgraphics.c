#include<stdio.h>

void parse_diff()
{
  char input[8];
  long long lines;
  scanf(
      "%d",
      &lines);
  long int line_array[lines];
  long int current_diff_line;
  int line_number;

  /*
Get the original image spec first, put it in the first section of every array
element.
  */
  for(
      line_number = 0;
      line_number < lines;
      line_number ++)
    {
      scanf(
          "%ld",
          &(line_array[line_number]));
    }
  /*
The changed image spec should go in [line_number][1] instead of [0], so that way
we can simply use the internal data structures of memory to compare the two and
spit out useful results.
  */
  for(
      line_number = 0;
      line_number < lines;
      line_number ++)
    {
      scanf(
          "%ld",
          &current_diff_line);
      if(
          line_array[line_number] !=
          current_diff_line)
        {
          printf(
              "%d, %d\n",
              line_number+1,
              current_diff_line);
        }
    }
}

int main()
{
  parse_diff();
  return 0;
}
