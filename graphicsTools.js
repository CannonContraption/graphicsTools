/*
  Built as a constructor in order to allow for multiple canvases.

  Pass a DOM canvas for graphics, that's it.
*/
function GraphicsTools(
    canvasobject)
{

    /*
      8 color pallete: set the rgba values for each color.
      This should facilitate coloring things in the canvas area.
    */
    this.foreground_pallete =
	[
	    'rgba(0,0,0,0)',
	    'rgba(0,0,0,1)',
	    'rgba(1,1,1,1)',
	    'rgba(255,   0,   0, 1)',
	    'rgba(  0, 255,   0, 1)',
	    'rgba(  0,   0, 255, 1)',
	    'rgba(255, 255,   0, 1)',
	    'rgba(  0, 255, 255, 1)'
	];
    /*
      Nothing uses this yet, but it can be useful for dual-draw routines on a
      single area of the page.
    */
    this.background_pallete =
	[
	    'rgba(0,0,0,0)',
	    'rgba(0,0,0,1)',
	    'rgba(1,1,1,1)',
	    'rgba(219, 217, 211, 1)',
	    'rgba(207, 147, 103, 1)',
	    'rgba( 91,  80,  80, 1)',
	    'rgba(255, 113,  79, 1)',
	    'rgba( 76,  63,  63, 1)'
	];

    /*
      The scale and size properties. graphicsTools.js does not assume a canvas
      size, instead leaving this up to whoever is drawing the image to decide
      the bounds of graphicsTools' clear area.
    */
    this.base_scale_x = 3;
    this.base_scale_y = 3;
    this.scale_x = 1;
    this.scale_y = 1;
    this.size_x = 70;
    this.size_y = 70;

    /*
      These are a couple essentials. We're going to need the canvas, and its
      2d context object. Thus, they should be class members.
    */
    this.canvas = canvasobject;
    this.context2d = this.canvas.getContext('2d');

    /*
      This should allow for setting bits and pieces of the pallete at a time.
    */
    this.set_foreground_pallete = function(
	palletespec)
    {
	Object.assign(this.foreground_pallete, palletespec);
    }

    this.set_background_pallete = function(
	palletespec)
    {
	Object.assign(this.background_pallete, palletespec);
    }

    /*
      Clears the area the graphicsTools is aware of.
    */
    this.clear = function()
    {
        this.context2d.clearRect(
            0,
            0,
            this.base_scale_x * this.scale_x * this.size_x,
            this.base_scale_y * this.scale_y * this.size_y);
    }

    /*
      The main render function- takes compiled render data from the convert
      command and then puts it on the canvas.
    */
    this.render_data = function(
	position_x,
	position_y,
	segment_width,
	segments_per_line,
	segment_count,
	data,
        diffpatch)
    {
        if(diffpatch == null)
            diffpatch = [];
	var current_y = position_y - 1;
	var current_x = position_x;
	var current_segment;
	for(
	    var segment_number = 0;
	    segment_number < segment_count;
	    segment_number ++)
	{
            current_segment = data[segment_number];
            for(
                var patch_number = 0;
                patch_number < diffpatch.length;
                patch_number ++)
            {
                if(diffpatch[patch_number][0] == segment_number+1)
                {
                    current_segment = diffpatch[patch_number][1];
                }
            }
	    if(segment_number % segments_per_line == 0)
	    {
		current_y ++;
		current_x = position_x;
	    }
	    //Loop through each pixel in segment.
	    for(
		var pixel_number = 0;
		pixel_number < segment_width;
		pixel_number ++)
	    {
		var color = current_segment % 8;
		current_segment = current_segment >>> 3;
		this.context2d.fillStyle = this.foreground_pallete[color];
		//draw rect at current_x current_y and increment x
		this.context2d.fillRect(
		    Math.ceil(current_x * this.base_scale_x * this.scale_x),
		    Math.ceil(current_y * this.base_scale_y * this.scale_y),
		    Math.ceil(this.base_scale_x * this.scale_x),
		    Math.ceil(this.base_scale_y * this.scale_y));
		current_x += 1;
	    }
	}
    }

    /*
      Renders playfield checkers.
    */
    this.render_checkers = function(
        color_1,
        color_2,
        x,
        y,
        width,
        height,
        segment_size)
    {
        var color_mode = 0;
        for(
            var current_y = 0;
            current_y < height-y;
            current_y ++)
        {
            for(
                var current_x = 0;
                current_x < width-x;
                current_x ++)
            {
                color_mode = (
                    (Math.floor(current_x / segment_size) % 2) ^
                        (Math.floor(current_y / segment_size) % 2));
                if(color_mode == 0)
                    this.context2d.fillStyle = color_1;
                else
                    this.context2d.fillStyle = color_2;
                this.context2d.fillRect(
                    current_x * this.base_scale_x * this.scale_x,
                    current_y * this.base_scale_y * this.scale_y,
                    this.base_scale_x * this.scale_x,
                    this.base_scale_y * this.scale_y);
            }
        }
    }

    /*
      Renders playfield stripes
    */
    this.render_stripes = function(
        color_1,
        color_2,
        x,
        y,
        width,
        height,
        segment_size,
        orientation)
    {
        var colormode = 0;
        if(orientation == true)
        {
            for(
                var current_x = 0;
                current_x < width+x;
                current_x ++)
            {
                if((current_x * 2 / segment_size) % 2 == 0)
                {
                    if(colormode == 0)
                    {
                        colormode = 1;
                        this.context2d.fillStyle = color_2;
                    }
                    else
                    {
                        this.context2d.fillStyle = color_1;
                        colormode = 0;
                    }
                }
                this.context2d.fillRect(
                    current_x * this.base_scale_x * this.scale_x,
                    y * this.base_scale_y * this.scale_y,
                    this.base_scale_x * this.scale_x,
                    height * this.base_scale_y * this.scale_y); 
            }
        }
        else
        {
            for(
                var current_y = 0;
                current_y < height - y;
                current_y ++)
            {
                if((current_y * 2 / segment_size) % 2 == 0)
                {
                    if(colormode == 0)
                    {
                        colormode = 1;
                        this.context2d.fillStyle = color_2;
                    }
                    else
                    {
                        colormode = 0;
                        this.context2d.fillStyle = color_1;
                    }
                }
                this.context2d.fillRect(
                    x * this.base_scale_x * this.scale_x,
                    current_y * this.base_scale_y * this.scale_y,
                    width * this.base_scale_x * this.scale_x,
                    this.base_scale_y * this.scale_y); 
            }
        }
    }
}
