#include<stdio.h>

/*
8colimage image format:
Number of segments
width of segment
segments per line
image data (as outputted by convert program)
*/

void drawline(
    int picturedata)
{
  int color = 0;
  for(
      int pixel = 7;
      pixel >= 0;
      pixel --)
    {
      color = picturedata % 8;
      printf(
          "\033[7;3%dm  \033[0m",
             color);
      picturedata = picturedata >> 3;
    }
}

void drawimage(
    int picture_segments,
    int segment_width,
    int segments_per_line,
    int picture[])
{
  for(
      int segment = 0;
      segment < picture_segments;
      segment ++)
    {
      if(segment % segments_per_line == 0) printf("\n");
      drawline(
          picture[segment]);
    }
  printf("\n");
}

int main(
    int argc,
    char* argv[])
{
  int picture_segments;
  scanf(
      "%d\n",
      &picture_segments);
  int segment_width;
  scanf(
      "%d\n",
      &segment_width);
  int segments_per_line;
  scanf(
      "%d\n",
      &segments_per_line);
  int picture[picture_segments];/* = {
    4793490, 4793490, 4793490,
    4793490, 4793490, 4793490,
    4793490, 4793490, 4793490,
    599186, 74752, 4793472,
    4269202, 4268178, 4793362,
    4727954, 4261010, 4793362,
    4727954, 4269202, 4793362,
    4727954, 74898, 4793472,
    6825106, 65554, 4793496,
    4990098, 4261010, 4793538,
    4727954, 4261010, 4793874,
    4306066, 4268178, 4797970,
    636562, 4269056, 4796626,
    4830866, 4793490, 4798162,
    636562, 4793488, 4798144,
    112274, 4792464, 4798144,
    112274, 589968, 4798170,
    898706, 4259840, 4798170,
    5093010, 6819840, 4794075,
    7189650, 7110659, 4793563,
    7185554, 7077896, 4793491,
    599186, 4194377, 4793490,
    2172050, 1545, 4793490,
    2172050, 9345, 4793488,
    599186, 74896, 4793488,
    4793490, 4793490, 4793490
    };*/
  for(
      int segment = 0;
      segment < picture_segments;
      segment ++)
    {
      scanf(
          "%d\n",
          &picture[segment]);
    }
  drawimage(
      picture_segments,
      segment_width,
      segments_per_line,
      picture);
  
  return 0;
}
