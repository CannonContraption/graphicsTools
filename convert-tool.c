#include<stdio.h>

void process_line(
    int linelength)
{
  int colorcode=0;
  int linecode=0;
  scanf(
      "%d",
      &colorcode);
  for(
      int col = 0;
      col < linelength;
      col ++)
    {
      linecode = linecode<<3;
      linecode += colorcode % 10;
      colorcode /= 10;
    }
  printf(
      "%d\n",
      linecode);
}

int main()
{
  int linecount = 0;
  int linelength = 16;
  scanf(
      "%d",
      &linecount);
  scanf(
      "%d",
      &linelength);
  for(
      int i = 0;
      i < linecount;
      i++)
    process_line(linelength);
  return 0;
}
